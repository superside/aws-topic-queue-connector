package com.superside.topicqueueconnector

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import

@Configuration
@Import(
    ExecutorsConfiguration::class
)
@ComponentScan
class TopicQueueConnectorModule
