package com.superside.topicqueueconnector

interface SnsPublisher {
    fun <T> send(topicName: String, subject: String, message: T)

    fun <T> sendToFifo(
        topicName: String, subject: String, message: T, messageGroupId: String, messageDeduplicationId: String
    )
}
