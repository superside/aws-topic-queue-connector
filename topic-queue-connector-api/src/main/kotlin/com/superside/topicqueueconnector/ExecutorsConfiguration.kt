package com.superside.topicqueueconnector

import org.apache.commons.lang3.Validate
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor
import java.util.concurrent.ScheduledThreadPoolExecutor
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit
import javax.annotation.PostConstruct
import javax.annotation.PreDestroy

/**
 *  Configures executors used in app in one place to make their management easier
 *  !!! Try to avoid configuring executors in other classes !!!
 */
@Configuration
class ExecutorsConfiguration {
    @Value("\${topic-queue-connector.sqs.consumer.executor.maxPoolSize:20}")
    var consumerExecutorMaxPoolSize: Int = 0

    @Value("\${topic-queue-connector.sqs.consumer.executor.queueCapacity:20}")
    var consumerExecutorQueueCapacity: Int = 0

    @Value("\${topic-queue-connector.sqs.consumer.executor.awaitTerminationSeconds:120}")
    var consumerExecutorAwaitTerminationSeconds: Int = 0

    @Value("\${topic-queue-connector.sqs.consumer.listener.maxPoolSize:30}")
    var consumerListenerMaxPoolSize: Int = 0

    @Value("\${topic-queue-connector.sqs.consumer.listener.queueCapacity:1}")
    var consumerListenerQueueCapacity: Int = 0

    @Value("\${topic-queue-connector.sqs.consumer.listener.awaitTerminationSeconds:20}")
    var consumerListenerAwaitTerminationSeconds: Int = 0

    private val sqsMessagesConsumerExecutor = ThreadPoolTaskExecutor()
    private val sqsMessagesListenerExecutor = ThreadPoolTaskExecutor()
    private val sqsMessageListenerScheduledExecutor = ScheduledThreadPoolExecutor(1) { r -> Thread(r, "sqs-listener-health-check-threads-%d") }

    @PostConstruct
    fun init() {
        initSQSMessagesConsumerExecutor()
        initSQSMessagesListenerExecutor()
    }

    private fun initSQSMessagesConsumerExecutor() {
        validateIsPositive(consumerExecutorMaxPoolSize)
        validateIsPositive(consumerExecutorQueueCapacity)
        validateIsPositive(consumerExecutorAwaitTerminationSeconds)

        sqsMessagesConsumerExecutor.corePoolSize = consumerExecutorMaxPoolSize
        sqsMessagesConsumerExecutor.maxPoolSize = consumerExecutorMaxPoolSize
        sqsMessagesConsumerExecutor.setQueueCapacity(consumerExecutorQueueCapacity)
        // if the queue is full, throttle task creation by running them in the caller's thread
        sqsMessagesConsumerExecutor.setRejectedExecutionHandler(ThreadPoolExecutor.CallerRunsPolicy())
        sqsMessagesConsumerExecutor.setAwaitTerminationSeconds(consumerExecutorAwaitTerminationSeconds)
        sqsMessagesConsumerExecutor.initialize()
        log.info("SQS consumer executor pool initialized with maxPoolSize=${sqsMessagesConsumerExecutor.maxPoolSize}")
    }

    private fun initSQSMessagesListenerExecutor() {
        validateIsPositive(consumerListenerMaxPoolSize)
        validateIsPositive(consumerListenerQueueCapacity)
        validateIsPositive(consumerListenerAwaitTerminationSeconds)

        sqsMessagesListenerExecutor.corePoolSize = consumerListenerMaxPoolSize
        sqsMessagesListenerExecutor.maxPoolSize = consumerListenerMaxPoolSize
        sqsMessagesListenerExecutor.setQueueCapacity(consumerListenerQueueCapacity)
        // if the queue is full, throttle task creation by running them in the caller's thread
        sqsMessagesListenerExecutor.setRejectedExecutionHandler(ThreadPoolExecutor.CallerRunsPolicy())
        sqsMessagesListenerExecutor.setAwaitTerminationSeconds(consumerListenerAwaitTerminationSeconds)
        sqsMessagesListenerExecutor.initialize()
        log.info("SQS listeners executor pool initialized with maxPoolSize=${sqsMessagesListenerExecutor.maxPoolSize}")
    }

    @PreDestroy
    fun shutdown() {
        sqsMessageListenerScheduledExecutor.shutdown()
        sqsMessagesConsumerExecutor.shutdown()
        sqsMessagesListenerExecutor.shutdown()
        if (!sqsMessageListenerScheduledExecutor.awaitTermination(10, TimeUnit.SECONDS)) {
            sqsMessageListenerScheduledExecutor.shutdownNow()
        }
    }

    @Bean
    fun sqsMessageListenerScheduledExecutor(): ScheduledThreadPoolExecutor {
        return sqsMessageListenerScheduledExecutor
    }

    @Bean
    fun sqsMessagesListenerExecutor(): ThreadPoolTaskExecutor {
        return sqsMessagesListenerExecutor
    }

    @Bean
    fun sqsMessagesConsumerExecutor(): ThreadPoolTaskExecutor {
        return sqsMessagesConsumerExecutor
    }

    private fun validateIsPositive(value: Int) {
        Validate.isTrue(value > 0)
    }

    companion object {
        private val log = LoggerFactory.getLogger(ExecutorsConfiguration::class.java)
    }
}
