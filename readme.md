# Topic Queue Connector

Connects topic to specified SQS queue, allocates DLQ and subscribes listener  method on queue events.<br>
Listeners and message processors execute in separate configurable pools, each minute health check job runs and reruns dead listeners.

* Creates input SQS queues and output SNS topics for the service
* Publishes events to topics
* Connect queues and topics
* Scans annotations and subscribe service methods on queue events
* Support FIFO topics and queues
* Manages listeners and message processing pools

<img src=".readme_images/usage_schema_example.jpg" width="800"/>

### Queues and topics
* If a topic or a queue doesn't exist, they will be created
* If topic or queue name ends on `.fifo` it will be configured as FIFO
* To subscribe queue on a topic it should have the same FIFO or not FIFO type
* DLQ named as `${queue_name}_dlq` will be created for each queue
* Queue or topic name can be passed as property name `\${property.path.topic-name}`

<img src=".readme_images/supported_connections_examples.jpg" width="800"/>

## Usage

### Include in maven
1. Copy and paste this inside your pom.xml dependencies block

    ```
    <dependency>
        <groupId>org.superside</groupId>
        <artifactId>topic-queue-connector-api</artifactId>
        <version>1.0.35-SNAPSHOT</version>
    </dependency>
    <dependency>
        <groupId>org.superside</groupId>
        <artifactId>topic-queue-connector</artifactId>
        <version>1.0.35-SNAPSHOT</version>
    </dependency>
    ```
2. Add the below to your pom.xml file.
    ```
    <repositories>
      <repository>
        <id>gitlab-maven</id>
        <url>https://gitlab.com/api/v4/projects/29417769/packages/maven</url>
      </repository>
    </repositories>

    <distributionManagement>
      <repository>
        <id>gitlab-maven</id>
        <url>https://gitlab.com/api/v4/projects/29417769/packages/maven</url>
      </repository>

      <snapshotRepository>
        <id>gitlab-maven</id>
        <url>https://gitlab.com/api/v4/projects/29417769/packages/maven</url>
      </snapshotRepository>
    </distributionManagement>
    ```

### Setup

1. import the module
    ```
    @Import(TopicQueueConnectorModule::class)
    ```

2. Optionally configure consumers and listeners pools
    ```
   topic-queue-connector.sqs.consumer:
       executor.maxPoolSize:20
       executor.queueCapacity:20
       executor.awaitTerminationSeconds:120
       listener.maxPoolSize:20
       listener.queueCapacity:1
       listener.awaitTerminationSeconds:20
   ```

3. Add Spring Boot AWS dependency in `pom.xml`
   ```
   <dependency>
       <groupId>org.springframework.cloud</groupId>
       <artifactId>spring-cloud-aws-autoconfigure</artifactId>
       <version>2.2.0.RELEASE</version>
   </dependency>
   ```

4. Add AWS related properties in application.yaml (might be different for prod and staging)
   ```
   cloud.aws:
       region:
           auto: false
           static: eu-west-2
       stack:
           auto: false
   ```
5. Add AWS client beans in configuration
   ```
    @Configuration
    class DefaultAWSClients(private val regionProvider: RegionProvider) {

    @Bean
    fun snsClient(): AmazonSNS {
        return AmazonSNSClientBuilder.standard()
            .also { it.region = regionProvider.region.name }
            .build()
    }

    @Bean
    fun sqsClient(): AmazonSQS {
        return AmazonSQSClientBuilder.standard()
            .also { it.region = regionProvider.region.name }
            .build()
    }

    @Bean
    fun s3Client(): AmazonS3 {
        return AmazonS3ClientBuilder.standard()
            .also { it.region = regionProvider.region.name }
            .build()
    }
      ```
}

### Create event-listener and auto-subscribe to a topic & event

Annotate a method in a listener with **@SQSBasedTopicListener**.

```
@Controller
class UserListener {

    @SQSBasedTopicListener(topic = "user", queue = "user-queue", subject = "created")
    fun userCreated(payload: UserCreated) {
        System.out.println("NEW USER CREATED -> $payload")
    }

    @SQSBasedTopicListener(topic = "user", queue = "user-queue", subject = "deleted")
    fun userDeleted(payload: UserDeleted) {
        System.out.println("NEW USER DELETED -> $payload")
    }
}
```

### Send a message to a topic
1. @Autowire SnsPublisher
2. Use `snsPublisher.send("topicName", "subject", event)` to publish into a reqular topics
   or `snsPublisher.sendFifo("topicName", "subject", event, "messageGroupId", "messageDeduplicationId")`
   to publish into a FIFO topics

Note: if topic isn't exist, it will be created automaticly

## Why we built this package

### Combining AWS SQS and SNS

AWS SQS is fully managed message queuing service at enables you to decouple and scale microservices. The problem is that
a message cannot be received by multiple receives at the same time, which means that the sender has to know what "
microservices" to send to.

AWS SNS is on the other hand a publish-subscribe system that can have multiple subscribers of the topic. The problem
with SNS is that if the subscriber is available at the moment, the message is lost.

The trick here is to make AWS SQS subscribe to an AWS SNS topic to get both the persistentness of the AWS SQS and
support multiple subscribers!

The current packages either handles this on low-level without any magic (AWS SDK), or only handle parts of the logic
with connecting a listener to AWS SQS (Spring Cloud AWS). Our package is inspired by AWS Cloud for Kafka, and is the
only package that handles the magic with auto-subscribing the AWS SQS queue to AWS SNS topics.

Source: https://medium.com/awesome-cloud/aws-difference-between-sqs-and-sns-61a397bf76c5

### Maximal message size

AWS SNS/SQS support messages with size less than 256 kb

## Note
`topic-queue-connector.extended-client.eventSourcingEnabled` is required for saving messages in S3.
It enables creating Firehose subscription per topic. **Must be enabled only for staging and prod!!**

## Todo

- To improve the performance, we can limit the subscription by filtering on events that the service is interested in,
  and ignoring the rest.
- Use jackson instead of moshi
- ...
