package com.superside.topicqueueconnector

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.superside.topicqueueconnector.TokenMasking.maskToken
import com.superside.topicqueueconnector.sqslistener.SQSMessageListenerFactory
import com.superside.topicqueueconnector.utils.SynchronizedMultiValueMap
import com.superside.topicqueueconnector.utils.TracerUtils
import datadog.trace.api.Trace
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import java.io.IOException
import java.util.concurrent.Executor

@Component
class MessagesProcessor(
    private val sqsMessagesConsumerExecutor: Executor,
    private val sqsMessageListenerFactory: SQSMessageListenerFactory
) {

    fun addListener(topicArnWithSubjectAndQueue: TopicArnWithSubjectAndQueue, listener: (payload: String) -> Unit) {
        topicEventListeners.add(topicArnWithSubjectAndQueue, listener)
        sqsMessageListenerFactory.addListener(
            queueUrl = topicArnWithSubjectAndQueue.queueUrl,
            messagesConsumer = { message -> processMessageAsync(message, topicArnWithSubjectAndQueue.queueUrl) }
        )
    }

    @Trace(resourceName = "MessagesProcessor.processMessageAsync")
    private fun processMessageAsync(awsMessageJob: AwsMessageJob, queueUrl: String) {
        log.info("Processing message [${awsMessageJob.message.messageId}]")
        sqsMessagesConsumerExecutor.execute {
            try {
                dispatch(awsMessageJob.message.body!!, queueUrl)
                awsMessageJob.onSuccess
                    .accept(awsMessageJob)
                TracerUtils.addTagToSpan(awsMessageJob.message.messageId)
            } catch (e: Exception) {
                if (e is InterruptedException) {
                    throw e
                }
                log.error("Failed to process message [${awsMessageJob.message.messageId}] from queue", e)
            }
        }
    }

    /**
     * Messageformat:
     *    {
     *      Type=Notification,
     *      MessageId=957d3aaa-b7c9-5200-900e-6300003035cd,
     *      TopicArn=arn:aws:sns:us-west-2:942828795263:user,
     *      Subject=user.created,
     *      Message={"userId": 55},
     *      Timestamp=2021-09-02T11:30:01.366Z,
     *      SignatureVersion=1,
     *      Signature=YHgnKUK/EKXMNwJ3FGfN3LbqS6Fq5bgaL5D3rFcUEHDhvx/37wo0hFrqPFxfos4s3YBj9Lw7DRkHeGQLw0JVxp1I0T8fII06lh/PqwSnUxb/bvEqcsqEqgMXTxFrD3mFVhHwThxYaObYXAX9iawGBe4XRSWdb3oZTiIRPiElAAYbnwBBCRN7bnjM+ccEn++RTghipfPCrPvkG3vSoWb/ljfm/91XkJmn1WNW2ga+QedbAQjZdOFYqiiJhWSM+swTSv2SBiWj/i9PLqwzruVrfU5bZ9a9O32ydjprK+aER232BrBvdV4irmRFz6+kd5+saR342zQjHldYKsymfSrqUg==,
     *      SigningCertURL=https://sns.us-west-2.amazonaws.com/SimpleNotificationService-010a507c1833636cd94bdb98bd93083a.pem,
     *      UnsubscribeURL=https://sns.us-west-2.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-west-2:942828795263:user:b694a5d4-a455-417f-af95-6ea9d0091f61
     *    }
     */
    @Trace(resourceName = "MessagesProcessor.dispatch")
    private fun dispatch(payload: String, queueUrl: String) {
        val map = try {
            val typeRef = object : TypeReference<Map<String, String>>() {}
            objectMapper.readValue(payload, typeRef)
        } catch (e: IOException) {
            log.error("Cannot deserialize message $payload", e)
            return
        }
        val topicArn = map["TopicArn"]!!
        val subject = map["Subject"]!!
        val rawBody = map["Message"]!!
        val listenersToDispatch = topicEventListeners.get(TopicArnWithSubjectAndQueue(topicArn, subject, queueUrl))
        if (listenersToDispatch.isNullOrEmpty()) {
            log.info("Skipping message due to missing listeners for topicArn:[$topicArn] subject:[$subject] queueUrl:[$queueUrl]")
            return
        }
        if (log.isDebugEnabled) {
            log.debug("Received SNS message with id ${map["MessageId"]}. $subject: ${rawBody.maskToken()}")
        }
        listenersToDispatch.forEach { it.invoke(rawBody) }
    }

    companion object {
        private val topicEventListeners: SynchronizedMultiValueMap<TopicArnWithSubjectAndQueue, (payload: String) -> Unit> =
            SynchronizedMultiValueMap()
        private val objectMapper = jacksonObjectMapper()
        private val log = LoggerFactory.getLogger(MessagesProcessor::class.java)
    }
}
