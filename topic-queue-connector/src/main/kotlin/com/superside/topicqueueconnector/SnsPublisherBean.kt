package com.superside.topicqueueconnector

import com.amazonaws.services.sns.AmazonSNS
import com.amazonaws.services.sns.model.PublishRequest
import com.amazonaws.services.sns.model.PublishResult
import com.amazonaws.services.sns.model.Topic
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.superside.topicqueueconnector.utils.MessageSize.Companion.validateMessageSize
import com.superside.topicqueueconnector.utils.TracerUtils
import org.slf4j.LoggerFactory
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder
import org.springframework.stereotype.Component

@Component
class SnsPublisherBean(
    private val amazonSNS: AmazonSNS,
    private val topicQueueConnectorService: TopicQueueConnectorService,
): SnsPublisher {

    override fun <T> send(topicName: String, subject: String, message: T) {
        val topic = topicQueueConnectorService.findOrCreateTopic(topicName)
        val publishResult = send(topic, subject, message)
        log.debug("Message [${publishResult.messageId}] was published to topic [$topicName] - [$topic] with subject [$subject]")
    }

    override fun <T> sendToFifo(
        topicName: String, subject: String, message: T, messageGroupId: String, messageDeduplicationId: String
    ) {
        val topic = topicQueueConnectorService.findOrCreateTopic(topicName)
        val publishResult = send(topic, subject, message, messageGroupId, messageDeduplicationId)
        log.debug("Message [${publishResult.messageId}] was published to FIFO topic [$topicName] - [$topic] with subject [$subject]")
    }

    private fun <T> send(
        topic: Topic,
        subject: String,
        message: T,
        messageGroupId: String? = null,
        messageDeduplicationId: String? = null
    ): PublishResult {
        val messageJson = objectMapper.writeValueAsString(message)
        val topicArn = topic.topicArn
        val request = PublishRequest(topicArn, messageJson, subject)
            .withMessageGroupId(messageGroupId)
            .withMessageDeduplicationId(messageDeduplicationId)
        request.validateMessageSize()
        return amazonSNS.publish(request)
            .also { TracerUtils.addTagToSpan(it.messageId) }
    }

    private companion object {
        val log = LoggerFactory.getLogger(SnsPublisher::class.java)
        val builder = Jackson2ObjectMapperBuilder().let {
            it.modulesToInstall(JavaTimeModule())
            it.featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
            it.featuresToDisable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
            it.serializationInclusion(JsonInclude.Include.NON_NULL)
        }
        val objectMapper = builder.build<ObjectMapper>()
    }
}
