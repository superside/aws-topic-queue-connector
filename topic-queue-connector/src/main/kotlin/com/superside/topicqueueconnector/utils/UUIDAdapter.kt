package com.superside.topicqueueconnector.utils

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import org.slf4j.LoggerFactory
import java.util.UUID

internal class UUIDAdapter {
    @ToJson
    fun toJson(value: UUID?): String? {
        return value?.toString()
    }

    @FromJson
    fun fromJson(value: String): UUID? {
        return UUID.fromString(value)
    }
}
