package com.superside.topicqueueconnector.utils

import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap

internal class SynchronizedMultiValueMap<K : Any, V> {
    private val map: MultiValueMap<K, V> = LinkedMultiValueMap()

    @Synchronized
    fun add(key: K, value: V) {
        map.add(key, value)
    }

    @Synchronized
    fun get(key: K): List<V>? = map[key]
}
