package com.superside.topicqueueconnector.utils

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.time.Instant

internal class InstantAdapter {
    @ToJson
    fun toJson(value: Instant?): String? {
        return value?.toString()
    }

    @FromJson
    fun fromJson(value: String): Instant? {
        return Instant.parse(value)
    }
}
