package com.superside.topicqueueconnector.utils

import com.amazonaws.services.sns.model.MessageAttributeValue
import com.amazonaws.services.sns.model.PublishRequest
import com.superside.topicqueueconnector.SnsPublisherException
import org.slf4j.LoggerFactory
import java.io.IOException
import java.io.OutputStream
import java.io.OutputStreamWriter
import java.io.Writer
import java.nio.charset.StandardCharsets

/**
 *  Util methods to calculate size and CountingOutputStream class are copied from software.amazon.payloadoffloading.Util to avoid aws libs versions conflicts
 */
class MessageSize {
    companion object {
        fun PublishRequest.validateMessageSize() {
            val messageSizeBytes = getSizeInBytes(this)
            val messageSizeKB = messageSizeBytes / 1024
            if (messageSizeKB > THRESHOLD_KB) {
                throw SnsPublisherException("Publishing message with size ${messageSizeKB}KB isn't possible since it's bigger then ${THRESHOLD_KB}KB threshold")
            } else if (messageSizeKB > WARNING_THRESHOLD_KB) {
                log.warn("Publishing message with size ${messageSizeKB}KB, it's close to ${THRESHOLD_KB}KB threshold")
            } else if (messageSizeKB > INFO_THRESHOLD_KB) {
                log.info("Publishing message with size ${messageSizeKB}KB, consider refactoring to lower size of such events")
            } else {
                log.debug("Publishing message with size ${messageSizeKB}KB")
            }
        }
        private const val THRESHOLD_KB = 256
        private const val WARNING_THRESHOLD_FRACTION = 0.7
        private const val WARNING_THRESHOLD_KB = THRESHOLD_KB * WARNING_THRESHOLD_FRACTION
        private const val INFO_THRESHOLD_FRACTION = 0.3
        private const val INFO_THRESHOLD_KB = THRESHOLD_KB * INFO_THRESHOLD_FRACTION

        private fun getSizeInBytes(publishRequest: PublishRequest): Long {
           return getStringSizeInBytes(publishRequest.message) + getMsgAttributesSize(publishRequest.messageAttributes)
        }

        private fun getMsgAttributesSize(msgAttributes: Map<String, MessageAttributeValue>): Long {
            return msgAttributes.entries
                .sumOf { getMessageAttributeSize(it.key, it.value) }
        }

        private fun getMessageAttributeSize(MessageAttributeKey: String, value: MessageAttributeValue): Long {
            var messageAttributeSize = getStringSizeInBytes(MessageAttributeKey)
            if (value.dataType != null) {
                messageAttributeSize += getStringSizeInBytes(value.dataType)
            }
            val stringVal = value.stringValue
            if (stringVal != null) {
                messageAttributeSize += getStringSizeInBytes(stringVal)
            }
            val binaryVal = value.binaryValue
            if (binaryVal != null) {
                messageAttributeSize += binaryVal.array().size.toLong()
            }
            return messageAttributeSize
        }

        private fun getStringSizeInBytes(str: String): Long {
            val counterOutputStream = CountingOutputStream()
            try {
                val writer: Writer = OutputStreamWriter(counterOutputStream, StandardCharsets.UTF_8)
                writer.write(str)
                writer.flush()
                writer.close()
            } catch (ioException: IOException) {
                throw SnsPublisherException("Failed to calculate the size of payload", ioException)
            }
            return counterOutputStream.totalSize
        }

        private val log = LoggerFactory.getLogger(MessageSize::class.java)
    }

    private class CountingOutputStream : OutputStream() {
        var totalSize: Long = 0
            private set

        override fun write(b: Int) {
            ++totalSize
        }

        override fun write(b: ByteArray) {
            totalSize += b.size.toLong()
        }

        override fun write(b: ByteArray, offset: Int, len: Int) {
            totalSize += len.toLong()
        }
    }
}
