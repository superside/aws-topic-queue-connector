package com.superside.topicqueueconnector

internal object TokenMasking {

    private val userTokenPattern = "xoxp-\\w+-\\w+-\\w+-\\w+".toRegex()
    private val botTokenPattern = "xoxb-\\w+-\\w+-\\w+".toRegex()

    fun String.maskToken(): String {
        if (this.isEmpty()) return this
        return this.replace(userTokenPattern, "****").replace(botTokenPattern, "****")
    }
}
