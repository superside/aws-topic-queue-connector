package com.superside.topicqueueconnector

data class TopicArnWithSubjectAndQueue(
    val topicArn: String,
    val subject: String,
    val queueUrl: String,
)
