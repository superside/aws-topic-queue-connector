package com.superside.topicqueueconnector.sqslistener

import com.amazonaws.services.sqs.AmazonSQS
import com.superside.topicqueueconnector.AwsMessageJob
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.event.EventListener
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor
import org.springframework.stereotype.Component
import java.util.concurrent.ScheduledThreadPoolExecutor
import java.util.concurrent.TimeUnit
import java.util.function.Consumer
import javax.annotation.PostConstruct
import javax.annotation.PreDestroy

@Component
class SQSMessageListenerFactory(
    private val amazonSQS: AmazonSQS,
    private val sqsMessageListenerScheduledExecutor: ScheduledThreadPoolExecutor,
    private val sqsMessagesListenerExecutor: ThreadPoolTaskExecutor,
) {
    private val listeners = mutableListOf<SQSMessageListener>()

    @Value("\${topic-queue-connector.sqs.consumer.retriesLimit:10}")
    private val retriesLimit: Int = 10

    @PostConstruct
    fun init() {
        sqsMessageListenerScheduledExecutor.scheduleAtFixedRate(
            { checkListenersHealth() },
            0,
            60,
            TimeUnit.SECONDS,
        )
    }

    @PreDestroy
    fun shutdown() {
        listeners.forEach { it.shutdown() }
    }

    @EventListener
    fun afterApplicationStarted(event: ApplicationReadyEvent) {
        listeners.forEach { it.afterApplicationStarted() }
    }

    @Synchronized
    private fun checkListenersHealth() {
        listeners.forEach { listener ->
            if (listener.healthy()) {
                log.debug("AWS SQS queue [${listener.queueUrl}] listener is healthy")
            } else {
                log.warn("Listener for queue [${listener.queueUrl}] isn't healthy. Refreshing it.")
                addListener(listener.queueUrl, listener.consumer)
            }
        }
    }

    @Synchronized
    fun addListener(queueUrl: String, messagesConsumer: Consumer<AwsMessageJob>) {
        if (sqsMessagesListenerExecutor.activeCount >= sqsMessagesListenerExecutor.maxPoolSize) {
            throw SQSMessageListenerFactoryException(
                "Fail to add listener - maximumPoolSize of sqsMessageListenerScheduledExecutor is reached",
            )
        }
        val listener = SQSMessageListener(
            amazonSQS = amazonSQS,
            queueUrl = queueUrl,
            consumer = messagesConsumer,
            retriesLimit = retriesLimit,
        )
        sqsMessagesListenerExecutor.execute(listener)
        listeners.add(listener)
        log.info("New listener job submitted for queue [$queueUrl]")
    }

    companion object {
        private val log = LoggerFactory.getLogger(SQSMessageListenerFactory::class.java)
    }
}
