package com.superside.topicqueueconnector.sqslistener

class SQSMessageListenerFactoryException(
    message : String,
    cause : Throwable? = null
): Exception(message, cause)
