package com.superside.example

import com.superside.topicqueueconnector.SnsPublisher
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import java.util.*
import java.util.concurrent.atomic.AtomicInteger

@RestController
class ExampleRestController(
    private val snsPublisher: SnsPublisher
) {

    @GetMapping("/success")
    fun sendSuccess() {
        snsPublisher.send(
            topicName = "test-topic",
            subject = "success",
            message = ExampleEvent("hello", counterSuccess.incrementAndGet())
        )
    }

    @GetMapping("/fail")
    fun sendFail() {
        snsPublisher.send(
            topicName = "test-topic",
            subject = "fail",
            message = mapOf("any" to "thing")
        )
    }

    @GetMapping("/success-fifo")
    fun sendSuccessFifo() {
        snsPublisher.sendToFifo(
            topicName = "test-topic.fifo",
            subject = "success",
            message = ExampleEvent("hello fifo", counterFifoSuccess.incrementAndGet()),
            messageGroupId = "1",
            messageDeduplicationId = UUID.randomUUID().toString()
        )
    }

    @GetMapping("/fail-fifo")
    fun sendFailFifo() {
        snsPublisher.sendToFifo(
            topicName = "test-topic.fifo",
            subject = "fail",
            message = mapOf("any" to "thing fifo"),
            messageGroupId = "1",
            messageDeduplicationId = UUID.randomUUID().toString()
        )
    }

    companion object {
        private val counterFifoSuccess = AtomicInteger(0)
        private val counterSuccess = AtomicInteger(0)
    }
}
