package com.superside.example

import com.superside.topicqueueconnector.annotation.SQSBasedTopicListener
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Controller

@Controller
open class ExampleListener {

    @SQSBasedTopicListener(topic = "test-topic", subject = "success", queue = "test-queue")
    open fun listener(event: ExampleEvent) {
        logger.info("triggered success listener with $event")
    }

    @SQSBasedTopicListener(topic = "test-topic", subject = "fail", queue = "test-queue")
    open fun failListener(map: Map<String, String>) {
        logger.info("triggered fail listener with $map")
        throw Exception("fails to test for retries")
    }

    @SQSBasedTopicListener(topic = "test-topic.fifo", subject = "success", queue = "test-queue.fifo")
    open fun fifoListener(event: ExampleEvent) {
        logger.info("triggered success FIFO listener with $event")
    }

    @SQSBasedTopicListener(topic = "\${example-queues.test-topic.fifo}", subject = "fail", queue = "\${example-queues.test-queue.fifo}")
    open fun failFifoListener(map: Map<String, String>) {
        logger.info("triggered fail FIFO listener with $map")
        throw Exception("fails to test for retries")
    }

    companion object {
        private val logger = LoggerFactory.getLogger(ExampleListener::class.java)
    }
}
